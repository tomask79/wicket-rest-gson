# Expose REST from Wicket Application #

To tell you the truth, I know that Wicket is able to expose REST API very long time, but I never had a time to explore it by myself, until now...And I have to say, good work.

## How Wicket exposes REST ##

First off all you need to include following project from wicket-stuff into your pom:


```
		<dependency>
			<groupId>org.wicketstuff</groupId>
			<artifactId>wicketstuff-restannotations-json</artifactId>
			<version>6.10.0</version>
		</dependency>
```
This project is bringing you possibility to expose resources over REST via Wicket's Resource API principles. You just need to create class inheriting from **AbstractRestResource** and annotate your methods with @**MethodMapping** annotation, which is more or less the same as @**RequestMapping** annotation you know from Spring MVC. Your AbstractRestResource implementation then uses Java Reflect API to read your annotations from your implementation via Java Reflect API and composes final response in the IResource.respond method...Easy and clear approach. Nice!

Documentation of all annotations is here: https://github.com/bitstorm/Wicket-rest-annotations

My simple example of RestResource implementation:


```
public class WeatherController extends GsonRestResource {
	private static final long serialVersionUID = 1L;
	
	private final List<WeatherDomain> weatherData = new ArrayList<WeatherDomain>();
	
	public WeatherController(){
		weatherData.add(new WeatherDomain("Brno", 34L));
		weatherData.add(new WeatherDomain("Ostrava", 30L));
		weatherData.add(new WeatherDomain("CeskaLipa", 25L));
	}
	
	@MethodMapping("/weatherdata")
	public List<WeatherDomain> getWeatherData() {
		return weatherData;
	}
}
```

As a final step, you need to mount your new controller exposing resources over REST protocol to your Wicket application:


```
.
.
mountResource("/", new ResourceReference("restReference") {
			private static final long serialVersionUID = 1L;
			
			WeatherController resource = new WeatherController();
			@Override
			public IResource getResource() {
				return resource;
			}
		});
```

"/" is a root context path to your controller. So if you annotated one of your methods with 	@MethodMapping("/weatherdata") as I did in the demo, then you need to call your REST with:

**http://host:port/weatherdata**

And that's all! 

To run this demo do the following:

* mvn clean install
* mvn jetty:run

and launch your browser at: http://localhost:8080/weatherdata...you should get


```
[{"city":"Brno","temperature":34},
{"city":"Ostrava","temperature":30},
{"city":"CeskaLipa","temperature":25}]
```