package com.mycompany.myproject.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.wicketstuff.rest.annotations.MethodMapping;
import org.wicketstuff.rest.resource.gson.GsonRestResource;

import com.mycompany.myproject.rest.domain.WeatherDomain;

public class WeatherController extends GsonRestResource {
	private static final long serialVersionUID = 1L;
	
	private final List<WeatherDomain> weatherData = new ArrayList<WeatherDomain>();
	
	public WeatherController(){
		weatherData.add(new WeatherDomain("Brno", 34L));
		weatherData.add(new WeatherDomain("Ostrava", 30L));
		weatherData.add(new WeatherDomain("CeskaLipa", 25L));
	}
	
	@MethodMapping("/weatherdata")
	public List<WeatherDomain> getWeatherData() {
		return weatherData;
	}
}
