package com.mycompany.myproject.rest.domain;

public class WeatherDomain {
	private String city;
	private Long temperature;
	
	/**
	 * @param city
	 * @param temperature
	 */
	public WeatherDomain(String city, Long temperature) {
		this.city = city;
		this.temperature = temperature;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Long getTemperature() {
		return temperature;
	}
	public void setTemperature(Long temperature) {
		this.temperature = temperature;
	}
}
